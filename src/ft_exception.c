/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exception.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 07:10:51 by vkryvono          #+#    #+#             */
/*   Updated: 2018/10/13 07:10:53 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_exception(char **buff, char **ptr, t_var *var)
{
	size_t	len;
	char	*str;

	*buff = ft_strnew(1);
	(*buff)[0] = **ptr;
	if (**ptr != '\0')
		(*ptr)++;
	if (var->width < 0)
		return (1);
	len = (size_t)((var->width > 1) ? var->width - 1 : 0);
	str = ft_strnew(len);
	if ((var->flags & F_ZERO) && !(var->flags & F_LJUST))
		str = ft_memset(str, '0', len);
	else
		str = ft_memset(str, ' ', len);
	*buff = (var->flags & F_LJUST) ? ft_strappend(buff, &str)
									: ft_strappend(&str, buff);
	return (ft_strlen(*buff));
}
