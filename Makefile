# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/09 18:37:18 by vkryvono          #+#    #+#              #
#    Updated: 2018/10/10 21:40:36 by vkryvono         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = libftprintf.a

INC_PATH	:=	./inc
SRC_PATH	:=	./src
OBJ_PATH	:=	./obj

RAW_SRC		=	$(shell find $(SRC_PATH) -type f | grep -E "\.c$$")
SRC			=	$(RAW_SRC:./src/%=%)
OBJ			=	$(addprefix $(OBJ_PATH)/,$(SRC:.c=.o))

CC			:=	gcc
FLAGS		:=	-Wall -Wextra -Werror
INC			=	-I./libft/includes -I$(INC_PATH)
LIBFT		=	-L./libft -lft

all: $(OBJ_PATH) $(NAME)

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)

$(OBJ_PATH)/%.o:$(SRC_PATH)/%.c
	$(CC) $(INC) -o $@ -c $< $(FLAGS)

$(NAME): $(OBJ)
	make -C libft
	ar rc $(NAME) $(OBJ) ./libft/objects/*.o
	ranlib $(NAME)

clean:
	rm -rf $(OBJ)
	make -C libft clean

fclean: clean
	rm -f $(NAME)
	make -C libft fclean

re: fclean all
