/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 10:56:49 by vkryvono          #+#    #+#             */
/*   Updated: 2018/09/15 22:36:34 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <wchar.h>
#include <stddef.h>
#include <limits.h>
#include <locale.h>

#include "libft.h"
#include "ft_printf.h"

int		main()
{
//	setlocale(LC_ALL, "en_US.UTF-8")
//	ft_printf("%x", 42);
//	ft_printf("%X", 42);
//	ft_printf("%x", 0);
//	ft_printf("%X", 0);
//	ft_printf("%x", -42);
//	ft_printf("%X", -42);
//	ft_printf("%x", 4294967296);
//	ft_printf("%X", 4294967296);
//	ft_printf("%10x", 42);
//	ft_printf("%-10x", 42);
//	ft_printf("%lx", 4294967296);
//	ft_printf("%llX", 4294967296);
//	ft_printf("%hx", 4294967296);
//	ft_printf("%hhX", 4294967296);
//	ft_printf("%jx", 4294967295);
//	ft_printf("%jx", 4294967296);
//	ft_printf("%jx", -4294967296);
//	ft_printf("%jx", -4294967297);
//	ft_printf("%llx", 9223372036854775807);
//	ft_printf("%llx", 9223372036854775808);
//	ft_printf("%010x", 542);
//	ft_printf("%-15x", 542);
//	ft_printf("%2x", 542);
//	ft_printf("%.2x", 5427);
//	ft_printf("%5.2x", 5427);
//	ft_printf("%#x", 42);
//	ft_printf("%#llx", 9223372036854775807);
//	ft_printf("%#x", 0);
//	ft_printf("%#x", 42);
//	ft_printf("%#X", 42);
//	ft_printf("%#8x", 42);
//	ft_printf("%#08x", 42);
//	ft_printf("%#-08x", 42);
	ft_printf("@moulitest: |%#.x %#.0x|\n", 0, 0);
	printf("orig: |%#.x %#.0x|\n", 0, 0);
	ft_printf("@moulitest: |%.x %.0x|\n", 0, 0);
	printf("orig: |%.x %.0x|\n", 0, 0);
	ft_printf("@moulitest: |%5.x %5.0x|\n", 0, 0);
	printf("orig: |%5.x %5.0x|\n", 0, 0);
//	ft_printf("%s", "abc");
//	ft_printf("%s", "this is a string");
//	ft_printf("%s ", "this is a string");
//	ft_printf("%s  ", "this is a string");
//	ft_printf("this is a %s", "string");
//	ft_printf("%s is a string", "this");
//	ft_printf("Line Feed %s", "\n");
//	ft_printf("%10s is a string", "this");
//	ft_printf("%.2s is a string", "this");
//	ft_printf("%5.2s is a string", "this");
//	ft_printf("%10s is a string", "");
//	ft_printf("%.2s is a string", "");
//	ft_printf("%5.2s is a string", "");
//	ft_printf("%-10s is a string", "this");
//	ft_printf("%-.2s is a string", "this");
//	ft_printf("%-5.2s is a string", "this");
//	ft_printf("%-10s is a string", "");
//	ft_printf("%-.2s is a string", "");
//	ft_printf("%-5.2s is a string", "");
//	ft_printf("%s %s", "this", "is");
//	ft_printf("%s %s %s", "this", "is", "a");
//	ft_printf("%s %s %s %s", "this", "is", "a", "multi");
//	ft_printf("%s %s %s %s string. gg!", "this", "is", "a", "multi", "string");
//	ft_printf("%s%s%s%s%s", "this", "is", "a", "multi", "string");
//	ft_printf("@moulitest: %s", NULL);
//	ft_printf("%.2c", NULL);
//	ft_printf("%c", 42);
//	ft_printf("%5c", 42);
//	ft_printf("%-5c", 42);
//	ft_printf("@moulitest: %c", 0);
//	ft_printf("%2c", 0);
//	ft_printf("null %c and text", 0);
//	ft_printf("% c", 0);
//	ft_printf("%o", 40);
//	ft_printf("%5o", 41);
//	ft_printf("%05o", 42);
//	ft_printf("%-5o", 2500);
//	ft_printf("%#6o", 2500);
//	ft_printf("%-#6o", 2500);
//	ft_printf("%-05o", 2500);
//	ft_printf("%-5.10o", 2500);
//	ft_printf("%-10.5o", 2500);
//	ft_printf("@moulitest: |%o|\n", 0);
//	printf("orig: |%o|\n", 0);
	ft_printf("@moulitest: |%.o %.0o|\n", 0, 0);
	printf("orig: |%.o %.0o|\n", 0, 0);
//	ft_printf("@moulitest: |%5.o %5.0o|\n", 0, 0);
//	printf("orig: |%5.o %5.0o|\n", 0, 0);
	ft_printf("@moulitest: |%#.o %#.0o|\n", 0, 0);
	printf("orig: |%#.o %#.0o|\n", 0, 0);
//	ft_printf("@moulitest: |%.10o|\n", 42);
//	printf("orig: |%.10o|\n", 42);
//	ft_printf("%d", 1);
//	ft_printf("the %d", 1);
//	ft_printf("%d is one", 1);
//	ft_printf("%d", -1);
//	ft_printf("%d", 4242);
//	ft_printf("%d", -4242);
//	ft_printf("%d", 2147483647);
//	ft_printf("%d", 2147483648);
//	ft_printf("%d", -2147483648);
//	ft_printf("%d", -2147483649);
//	ft_printf("% d", 42);
//	ft_printf("% d", -42);
//	ft_printf("%+d", 42);
//	ft_printf("%+d", -42);
//	ft_printf("%+d", 0);
//	ft_printf("% +d", 42);
//	ft_printf("% +d", -42);
//	ft_printf("%+ d", 42);
//	ft_printf("%+ d", -42);
//	ft_printf("%  +d", 42);
//	ft_printf("%  +d", -42);
//	ft_printf("%+  d", 42);
//	ft_printf("%+  d", -42);
//	ft_printf("% ++d", 42);
//	ft_printf("% ++d", -42);
//	ft_printf("%++ d", 42);
//	ft_printf("%++ d", -42);
//	ft_printf("%0d", -42);
//	ft_printf("%00d", -42);
//	ft_printf("%5d", 42);
//	ft_printf("%05d", 42);
//	ft_printf("%0+5d", 42);
//	ft_printf("%5d", -42);
//	ft_printf("%05d", -42);
//	ft_printf("%0+5d", -42);
//	ft_printf("%-5d", 42);
//	ft_printf("%-05d", 42);
//	ft_printf("%-5d", -42);
//	ft_printf("%-05d", -42);
//	ft_printf("%hd", 32767);
//	ft_printf("%hd", 32768);
//	ft_printf("%hhd", 127);
//	ft_printf("%hhd", 128);
//	ft_printf("%hhd", -128);
//	ft_printf("%hhd", -129);
//	ft_printf("%ld", 2147483647);
//	ft_printf("%ld", -2147483648);
//	ft_printf("%ld", 2147483648);
//	ft_printf("%ld", -2147483649);
//	ft_printf("%lld", 9223372036854775807);
//	ft_printf("%lld", -9223372036854775808);
//	ft_printf("%jd", 9223372036854775807);
//	ft_printf("%jd", -9223372036854775808);
//	ft_printf("%zd", 4294967295);
//	ft_printf("%zd", -0);
//	ft_printf("%zd", -1);
//	ft_printf("%d", 1);
//	ft_printf("%d %d", 1, -2);
//	ft_printf("%d %d %d", 1, -2, 33);
//	ft_printf("%d %d %d %d", 1, -2, 33, 42);
//	ft_printf("%d %d %d %d gg!", 1, -2, 33, 42, 0);
//	ft_printf("%4.15d", 42);
//	ft_printf("%.2d", 4242);
//	ft_printf("%.10d", 4242);
//	ft_printf("%10.5d", 4242);
//	ft_printf("%-10.5d", 4242);
//	ft_printf("% 10.5d", 4242);
//	ft_printf("%+10.5d", 4242);
//	ft_printf("%-+10.5d", 4242);
//	ft_printf("%03.2d", 0);
//	ft_printf("%03.2d", 1);
//	ft_printf("%03.2d", -1);
//	ft_printf("@moulitest: %.10d", -42);
//	ft_printf("@moulitest: %.d %.0d", 42, 43);
//	ft_printf("@moulitest: %.d %.0d", 0, 0);
//	ft_printf("@moulitest: %5.d %5.0d", 0, 0);
//	ft_printf("%u", 0);
//	ft_printf("%u", 1);
//	ft_printf("%u", -1);
//	ft_printf("%u", 4294967295);
//	ft_printf("%u", 4294967296);
//	ft_printf("%5u", 4294967295);
//	ft_printf("%15u", 4294967295);
//	ft_printf("%-15u", 4294967295);
//	ft_printf("%015u", 4294967295);
//	ft_printf("% u", 4294967295);
//	ft_printf("%+u", 4294967295);
//	ft_printf("%lu", 4294967295);
//	ft_printf("%lu", 4294967296);
//	ft_printf("%lu", -42);
//	ft_printf("%llu", 4999999999);
//	ft_printf("%ju", 4999999999);
//	ft_printf("%ju", 4294967296);
//	ft_printf("%U", 4294967295);
//	ft_printf("%hU", 4294967296);
//	ft_printf("%U", 4294967296);
//	ft_printf("@moulitest: %.5u", 42);
	return (0);
}